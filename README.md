# How to clone de project and start the container docker

## Clone the project

```console
git clone git@gitlab.com:buratti-experiments/next-level-week/next-level-week-web.git
```

## Install the packages of dependencies of the project

```console
npm i
```

## Create image docker locale

```console
docker build -t registry.gitlab.com/buratti-experiments/next-level-week/next-level-week-web .
```

## Run container docker

```console
docker run -it -p 9001:3000 -v $(pwd):/app registry.gitlab.com/buratti-experiments/next-level-week/next-level-week-web
```

---

# Create project with structure node

```console
npm init -y
```

# Install the packages of dependencies of the your project

```console
npm i
```

# Create the file `Dockerfile` with the configurations

```yaml
FROM node:9-slim
WORKDIR /app
COPY package.json /app
RUN npm install
COPY . /app
EXPOSE 3000
CMD [ "npm", "start" ]
```

# Create de image docker

```console
docker build -t <project-name-here> .
```

# Run your container docker

```console
docker run -it -p 9000:3000 -v $(pwd):/app <project-name-here>
```

# Project configuration scripts

## Create a new `react` project by executing the command

```console
npx create-react-app <project-name-here>
```

## Run the project

```console
npm start
```
